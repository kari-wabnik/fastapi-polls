from fastapi import FastAPI
from fastapi_polls.models import Elements
from datetime import datetime
import uvicorn

app = FastAPI()

history = {}


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.post("/add")
async def add(elements: Elements):
    result = {"a": elements.a, "b": elements.b, "sum": elements.sum()}
    timestamp = datetime.now()
    history[timestamp] = result
    return result


@app.get("/history")
async def get_history():
    return history


def entypoint():
    uvicorn.run("fastapi_polls.main:app", host="0.0.0.0", port=8088)


if __name__ == "__main__":
    entypoint()
