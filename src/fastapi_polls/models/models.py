from pydantic import BaseModel


class Elements(BaseModel):
    a: int
    b: int

    def sum(self):
        return self.a + self.b
